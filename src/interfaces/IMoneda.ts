export default interface IMoneda {
    valor: string,
    cantidad: number,
    total: number
}