import styled from 'styled-components'

export const Main = styled.main`
text-align: center;
width: 280px;
min-height: 400px;
background-image: linear-gradient(#f6f6f7, #ffffff);
margin: 40px auto;
padding: 20px 40px;
border-radius: 7px;
font-family: "Open Sans", Helvetica, Arial, sans-serif;
line-height: 1.2;
box-shadow: 0px 0px 10px rgba(6, 7, 16, .1);
border: 1px solid #dedede;
font-size: 12px;
`;

export const Title = styled.h1`
text-align: center;
font-weight: 700;
font-size: 2.5em;
color: #2d2d2d;
`;

export const Subtitle = styled.h2`
text-align: left;
font-weight: 500;
font-size: 1.5em;
color: #2d2d2d;
margin: 20px auto 10px;
`;

export const Total = styled.h3`
text-align: right;
font-weight: 500;
font-size: 1.5em;
font-weight: 700;
color: #111;
margin: 20px 0 0;
`;

export const Label = styled.label`
text-align: left;
font-weight: 500;
font-size: 1.5em;
color: #2d2d2d;
margin-top: 20px;
display: inline-block;
width: 22%;
`;

export const Select = styled.select`
  display: inline-block;
  width: 75%;
  height: calc(1.5em + 0.75rem + 2px);
  padding: 0.375rem 0.75rem;
  font-size: 1rem;
  font-weight: 400;
  line-height: 1.5;
  color: #495057;
  background-color: #fff;
  background-clip: padding-box;
  border: 1px solid #ced4da;
  border-radius: 0.25rem;
  margin: 0 0 20px 2%;
`;

export const List = styled.ul`
list-style: none;
padding-left:10px;
text-align: left;
font-size: 1.2em;
& li {
  line-height: 1.5;
}
`;

export const Line = styled.hr`
margin-top: 20px;
border: 0;
border-bottom: 1px dashed #ccc;
box-shadow: none;
`;

export const Primary = styled.button`
display: inline-block;
font-weight: 700;
text-align: center;
vertical-align: middle;
background-color: transparent;
border: 1px solid transparent;
padding: 0.375rem 1.25rem;
font-size: 1rem;
line-height: 1.5;
border-radius: 0.25rem;
background: rgb(0, 82, 204) linear-gradient(180deg, rgb(0, 82, 204), rgb(0, 60, 150)) repeat-x;
color: #f7f7f7;
cursor: pointer;
&:hover {
  background: #839496;
  color: white;
}
`;