import React, { Fragment } from 'react'
import IMoneda from '../interfaces/IMoneda'
import { Subtitle, List, Total, Line } from '../styles';

export const Totals: React.FunctionComponent<{montos: IMoneda[]}> = ({ montos }) => {
    return (
        <Fragment>
            <div className="form-group">
                <Subtitle>Total x denominación</Subtitle>
                <List>
                    {
                        montos.map((m: IMoneda, index: number) => {
                            return <li key={index}><strong>{m.valor}</strong> [<span>x{m.cantidad}</span>] total: $<span>{m.total}</span></li>
                        })
                    }
                </List>
                <Line />
                <Total>Total: $
                    {
                        montos.reduce((sum: number, arr: IMoneda) => sum + (arr.total || 0), 0)
                    }
                </Total>
            </div>
        </Fragment>
    );

}
