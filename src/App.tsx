import React, { useState, useRef } from 'react'
import IMoneda from './interfaces/IMoneda'
import { init } from './data/init.json'
import { Main, Title, Subtitle, Select, Label, Primary } from './styles';
import { Totals } from './components/Totals'

type FormElement = React.FormEvent<HTMLFormElement>

function App(): JSX.Element {
    const [ valor, setValor ] = useState<string>('100');
    const [ montos, setMontos ] = useState<IMoneda[]>(init);
    const selMoneda = useRef<HTMLSelectElement>(null)
    
    const handleSubmit = (e: FormElement) => {
        
        agregarMoneda(valor)
        e.preventDefault()
    }

    const agregarMoneda = (moneda:string) => {
        let valIndex: number = montos.findIndex(x => x.valor === moneda)
        let newCantidad:number = montos[valIndex].cantidad + 1
        let newSet = {
            valor: moneda,
            cantidad: newCantidad,
            total: parseInt(moneda) * newCantidad
        }
    
        montos.splice(valIndex, 1, newSet)
        
        setMontos([...montos])
    }

    return (
        <Main className="container">
            <form onSubmit={handleSubmit}>
            <Title>Mi AlcancíaApp</Title>
                <Subtitle>
                Agregar moneda
                </Subtitle>
                <div className="form-group">
                    <Label>Valor: $</Label>
                    <Select className="form-control" onChange={e=> setValor(e.target.value)} 
                        id="monedas"
                        value={valor} 
                        ref={selMoneda}>{
                        init.map((m:any, i:number) => (
                                <option value={m.valor} key={i}>{m.valor}</option>
                            )
                        )}
                    </Select>
                    <Primary className="btn btn-primary" type="submit">Agregar</Primary>
                </div>
                <Totals montos={montos} />
            </form>
        </Main>
    );
}

    export default App;
